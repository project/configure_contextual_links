<?php

namespace Drupal\configure_contextual_links;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage custom contextual links configurations.
 */
class ConfigureContextualLinksManager implements ContainerInjectionInterface {
  /**
   * Contextual link configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ConfigureContextualLinksManager constructor.
   *
   * @param Drupal\Core\Config\ConfigFactory $config
   *   The configuration manager.
   */
  public function __construct(ConfigFactory $config) {
    $this->config = $config->get('configure_contextual_links.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * Perform contextual link alterations.
   *
   * @see configure_contextual_links.module
   * @see hook_contextual_links_alter()
   */
  public function alter(array &$links, $group, array $route_parameters) {
    if (!$this->config) {
      return;
    }

    // Disable contextual links.
    $disabled = $this->config->get('disabled');
    if (!empty($disabled)) {
      $links_to_disable = array_intersect(array_keys($links), $disabled);
      foreach ($links_to_disable as $key) {
        unset($links[$key]);
      }
    }

    // Relabel contextual links.
    $relabel = $this->config->get('relabel');
    if (!empty($relabel)) {
      foreach ($relabel as $relabel_key => $new_label) {
        // Replace "-" with "." so key matches Drupal format.
        $key = str_replace('-', '.', $relabel_key);
        if (isset($links[$key])) {
          $links[$key]['title'] = $new_label;
        }
      }
    }
  }

}
