<?php

namespace Drupal\configure_contextual_links\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\ContextualLinkManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Configure Contextual Links settings form.
 */
class ContextualLinkSettingsForm extends ConfigFormBase {

  /**
   * A contextual link plugin manager to deal with contextual links.
   *
   * @var \Drupal\Core\Menu\ContextualLinkManager
   */
  protected $contextualLinkManager;

  /**
   * Constructs a ContextualLinkSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Menu\ContextualLinkManager $contextual_link_manager
   *   A contextual link plugin manager to deal with contextual links.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ContextualLinkManager $contextual_link_manager) {
    parent::__construct($config_factory);
    $this->contextualLinkManager = $contextual_link_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.menu.contextual_link'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configure_contextual_links_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['configure_contextual_links.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->messenger()->addMessage($this->t(
      'Contextual links get stored in browser session data. You may need to 
      restart your browser or clear your session data to see this configuration 
      take effect.'
    ));

    $config = $this->config('configure_contextual_links.settings');
    $form = parent::buildForm($form, $form_state);

    $definitions = $this->contextualLinkManager->getDefinitions();

    $options = [];

    foreach ($definitions as $definition) {
      $options[$definition['id']] = $definition['group'] . ': ' . $definition['title'];
    }

    $form['configure_options'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-disabled-tab',
    ];

    $form['disabled_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Disable Contextual Links'),
      '#group' => 'configure_options',
    ];

    $form['disabled_tab']['disabled'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#required' => FALSE,
      '#default_value' => $config->get('disabled') ?? [],
    ];

    $form['relabel_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Relabel Contextual Links'),
      '#group' => 'configure_options',
    ];

    $form['relabel_tab']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Leave field blank to use default label.'),
    ];
    $form['relabel_tab']['relabel'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['relabel_tab']['relabel']['divider'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
      '#weight' => '0',
    ];

    $relabel = $config->get('relabel');
    foreach ($options as $key => $label) {
      $relabel_key = $this->convertKeyToConfig($key);

      $title = $this->t('Relabel @c_link', ['@c_link' => $label]);

      $default = $relabel[$relabel_key] ?? '';
      $weight = !empty($default) ? '-1' : '1';
      $form['relabel_tab']['relabel'][$relabel_key] = [
        '#type' => 'textfield',
        '#title' => $title,
        '#default_value' => $default,
        '#weight' => $weight,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('configure_contextual_links.settings');
    $values = $form_state->getValues();

    if (isset($values['disabled'])) {
      $disabled = $this->cleanSelectedItems($values['disabled']);
      $config->set('disabled', $disabled);
    }
    else {
      $config->set('disabled', []);
    }

    if (isset($values['relabel'])) {
      $relabel = $this->cleanSelectedItems($values['relabel'], TRUE);
      $config->set('relabel', $relabel);
    }
    else {
      $config->set('relabel', []);
    }

    // $relabel = $this->cleanSelectedItems($values['relabel']);
    $config->save();
    $this->contextualLinkManager->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }

  /**
   * Pull selected keys from multiselect data.
   */
  protected function cleanSelectedItems(array $values, $include_value = FALSE) {
    $clean_values = [];
    foreach ($values as $key => $value) {
      if (empty($value)) {
        continue;
      }
      if ($include_value) {
        $clean_values[$key] = $value;
      }
      else {
        $clean_values[] = $key;
      }
    }
    return $clean_values;
  }

  /**
   * Replace '.' with '-' so the key can be used in configuration storage.
   */
  protected function convertKeyToConfig($key) {
    return str_replace('.', '-', $key);
  }

}
